let readCsvFile = require('../../util.js')
let fs = require('file-system')

readCsvFile.readCsvFile(getAtheMatchIds)
let ids = [];

function getEconimicalBowlers(data) {
    try {
        let bowlers = {}
        for (let index = 0; index < data.length; index++) {
            if (ids.includes(data[index]['match_id'])){
                    if (!bowlers[data[index]['bowler']]) {
                        bowlers[data[index]['bowler']] = [0, 0] //runs,balls
                    }
                    bowlers[data[index]['bowler']][0] =  bowlers[data[index]['bowler']][0]+Number(data[index]['total_runs'])-Number(data[index]['legbye_runs'])-Number(data[index]['bye_runs'])
                    bowlers[data[index]['bowler']][1] =  bowlers[data[index]['bowler']][1] +( Number(data[index]['ball']) <=6 ? 1:0)
                }
        }
        for (let bowler in bowlers) {
            bowlers[bowler] = bowlers[bowler][0] / (bowlers[bowler][1]/6)
        }

        const sortedBolers = Object.fromEntries(
            Object.entries(bowlers).sort((b1, b2) => b1[1] - b2[1])
        );
        let index = 0
        let topBowlers = {}
        for (let bowle in sortedBolers) {
            if (index < 10) {
                topBowlers[bowle] = sortedBolers[bowle].toFixed(3)
            }
            index = index + 1
        }
        console.log(topBowlers)
        fs.writeFile('src/public/outputs/economical_bowler.json', JSON.stringify(topBowlers),)

    } catch (err) {
        console.log(err)
    }
}

function getAtheMatchIds(data, year = 2015) {
    try {
        let matchIds = []
        for (let index = 0; index < data.length; index++) {
            let tempDate = new Date(data[index]['date'])
            tempDate = tempDate.getFullYear()
            if (tempDate == year) {
                matchIds.push(data[index]['id'])
            }
        }
        ids = matchIds
        readCsvFile.readCsvFile(getEconimicalBowlers, 'src/data/deliveries.csv')
    } catch (error) {
        console.log(error)
    }
}