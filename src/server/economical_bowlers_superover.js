let readCsvFile = require('../../util.js')
let fs = require('file-system')
readCsvFile.readCsvFile(getMostEconomicBowlerInSuperOver, 'src/data/deliveries.csv')


function getMostEconomicBowlerInSuperOver(data) {
    try {
        let bowlers = {}
        for (let index = 0; index < data.length; index++) {
            if (data[index]['is_super_over'] == 1) {
                if (!bowlers[data[index]['bowler']]) {
                    bowlers[data[index]['bowler']] = [0, 0]
                }
                bowlers[data[index]['bowler']][0] = bowlers[data[index]['bowler']][0] + Number(data[index]['total_runs']) - Number(data[index]['legbye_runs']) - Number(data[index]['bye_runs'])
                bowlers[data[index]['bowler']][1] = bowlers[data[index]['bowler']][1] + (Number(data[index]['ball']) <= 6 ? 1 : 0)
            }
        }
        for (let bowler in bowlers) {
            bowlers[bowler] = (((bowlers[bowler][0]) / (bowlers[bowler][1]) / 6) * 6)
        }
        let min = Math.min(...Object.values(bowlers))
        let requireBowler = {}
        for (let bowler in bowlers) {
            if (bowlers[bowler] == min) {
                requireBowler[bowler] = min
            }
        }
        console.log(requireBowler)
        fs.writeFile('src/public/outputs/economical_bowler_superover.json', JSON.stringify(requireBowler),)

    } catch (error) {
        console.log(error)
    }

}