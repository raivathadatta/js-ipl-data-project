let readCsvFile = require('../../util.js')
let fs = require('file-system')

readCsvFile.readCsvFile(getAtheMatchIds)

let ids = []
function extraRunsGivenYear(data, year = 2016) {
    try {
        if (!Array.isArray(data)) {
            throw 'input miss match expected array of objects'
        }
        let extraRuns = {}
        for (let index = 0; index < data.length; index++) {
            if (data[index]['match_id'] >= ids[0] && data[index]['match_id'] <= ids[1]) {

                if (!extraRuns[data[index]['batting_team']]) {
                    extraRuns[data[index]['batting_team']] = 0
                }
                extraRuns[data[index]['batting_team']] = Number(extraRuns[data[index]['batting_team']]) + Number(data[index]['extra_runs'])
            }
        }
        fs.writeFile('src/public/outputs/extra_runs_in_year.json', JSON.stringify(extraRuns),)

    } catch (error) {
        console.log(error)
    }

}

function getAtheMatchIds(data, year = 2016) {
    try {
        let matchIds = []
        for (let index = 0; index < data.length; index++) {
            let season = new Date(data[index]['date'])
            season = season.getFullYear()
            if (season == year) {
                matchIds.push(data[index]['id'])
            }
        }
        ids = [matchIds[0], matchIds[matchIds.length - 1]]
        readCsvFile.readCsvFile(extraRunsGivenYear, 'src/data/deliveries.csv')
    } catch (error) {
        console.log(error)
    }
}