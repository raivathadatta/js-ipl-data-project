
let readCsvFile = require('../../util.js')
let fs = require('file-system')

readCsvFile.readCsvFile(getHigestManOfTheMathsForAllTheSeasons)

function getHigestManOfTheMathsForAllTheSeasons(data) {

    try {
        let manOfMatches = {}

        for (let index = 0; index < data.length; index++) {
            let season = new Date(data[index]['date'])
            season = season.getFullYear()
            if (season) {
                if (!manOfMatches[season]) {
                    manOfMatches[season] = {}
                }
                if (!manOfMatches[season][data[index]['player_of_match']]) {
                    manOfMatches[season][data[index]['player_of_match']] = 0
                }
                manOfMatches[season][data[index]['player_of_match']] = 1 + manOfMatches[season][data[index]['player_of_match']]

            }
        }
        let manofMatchsPerYear = {};
        for (let year in manOfMatches) {
            if (year != 'NaN') {
                let max = Math.max(...Object.values(manOfMatches[year]))
                let values = Object.entries(manOfMatches[year])
                let manOfTheMatch = []

                for (let index = 0; index < values.length; index++) {
                    if (values[index][1] == max) {
                        manOfTheMatch.push(values[index])
                    }
                }
                manOfTheMatch = Object.fromEntries(manOfTheMatch)
                manofMatchsPerYear[year] = manOfTheMatch
            }
        }
        console.log(manofMatchsPerYear)
        fs.writeFile('src/public/outputs/man_of_match_per_year.json', JSON.stringify(manofMatchsPerYear),)

    } catch (error) {
        console.log(error)
    }

}