
let readCsvFile = require('../../util.js')
let fs = require('file-system')

readCsvFile.readCsvFile(matchesPerYear)

function matchesPerYear(data) {
    try {
        if (!Array.isArray(data)) {
            throw "input miss Match please check"
        }
        let dataMatchesPerYear = {}
        for (let index = 0; index < data.length; index++) {

            let season = new Date(data[index]['date'])
            season = season.getFullYear()
            if (season) {

                if (!dataMatchesPerYear[season]) {
                    dataMatchesPerYear[season] = 1
                } else {
                    dataMatchesPerYear[season] = 1 + dataMatchesPerYear[season]
                }
            }
        }
        console.log(dataMatchesPerYear)
        fs.writeFile('src/public/outputs/match-per-year.json', JSON.stringify(dataMatchesPerYear),)
    } catch (err) {
        console.log(err)
    }
}