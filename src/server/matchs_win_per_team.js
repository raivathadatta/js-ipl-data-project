// Number of matches won per team per year in IPL
let readCsvFile = require('../../util.js')
let fs = require('file-system')
readCsvFile.readCsvFile(matchesWinPerYear)

function matchesWinPerYear(data) {
    try {
        if (!Array.isArray(data)) {
            throw "input miss Match please check"
        }
        let matches = {}
        for (let index = 0; index < data.length; index++) {
            let season = new Date(data[index]['date'])
            season = season.getFullYear()
            if(season != 'NaN'){
                if(data[index]['winner']){
                    if (!matches[data[index]['winner']]) {
                        matches[data[index]['winner']] = {}
                    }
                    if (!matches[data[index]['winner']][season]) {
                        matches[data[index]['winner']][season] = 0
                    }
                    matches[data[index]['winner']][season] = 1 + matches[data[index]['winner']][season]
         
                }
            }
        }
        console.log(matches)
        fs.writeFile('src/public/outputs/match-win-per-team-per-year.json', JSON.stringify(matches))

    } catch (error) {
        console.log(error)
    }
}

