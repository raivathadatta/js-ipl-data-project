let readCsvFile = require('../../util.js')
let fs = require('file-system')

readCsvFile.readCsvFile(playerDismissedByAnotherPlayer, 'src/data/deliveries.csv')

function playerDismissedByAnotherPlayer(data) {
    try {
        let playerDissmissedByAnother = {}
        let maxDissmissed=[];
        for (let index = 0; index < data.length; index++) {
            if (data[index]['player_dismissed']) {
                if (!playerDissmissedByAnother[data[index]['player_dismissed']]) {
                    playerDissmissedByAnother[data[index]['player_dismissed']] = {}
                }
                let tempPlayer = data[index]['fielder']  ? data[index]['fielder']: data[index]['bowler']
                
                if (!playerDissmissedByAnother[data[index]['player_dismissed']][tempPlayer]) {
                    playerDissmissedByAnother[data[index]['player_dismissed']][tempPlayer] = 0
                }
                playerDissmissedByAnother[data[index]['player_dismissed']][tempPlayer] = 1 + playerDissmissedByAnother[data[index]['player_dismissed']][tempPlayer]
            }
         
            }
            let maxPalyerDissmissedByOther ={}
            for (let player in playerDissmissedByAnother) {
            
                maxDissmissed.push( Math.max(...Object.values(playerDissmissedByAnother[player])))
            }
            maxDissmissed=Math.max(...maxDissmissed)

            for(let batter in playerDissmissedByAnother ){
                for(let bowler in playerDissmissedByAnother[batter]){
                    if(playerDissmissedByAnother[batter][bowler] == maxDissmissed){
                        console.log(batter)
                        if(! maxPalyerDissmissedByOther[batter]){
                            maxPalyerDissmissedByOther[batter]={}
                        }
                        maxPalyerDissmissedByOther[batter][bowler]=maxDissmissed
                    }
                }
             
            }
        console.log(maxPalyerDissmissedByOther)
        fs.writeFile('src/public/outputs/player_dissmissed_by_another_player.json', JSON.stringify(maxPalyerDissmissedByOther))

    } catch (error) {
        console.log(error)
    }
}
