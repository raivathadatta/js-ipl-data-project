let readCsvFile = require('../../util.js')
let fs = require('file-system')

readCsvFile.readCsvFile(getTheMatchIds)

let ids = {}

function getStrikeRateOfEveryPlayer(data) {
    try {
        let strikeRate = {}
        for (let year in ids) {
            for (let index = 0; index < data.length; index++) {
                if (ids[year].includes(data[index]['match_id'])) {
                    if (!strikeRate[year]) {
                        console.log(year)
                        strikeRate[year] = {}
                    }
                    if (!strikeRate[year][data[index]['batsman']]) {
                        strikeRate[year][data[index]['batsman']] = [0, 0]//runs,balls
                    }
                    if (data[index]['wide_runs'] == 0) {
                        strikeRate[year][data[index]['batsman']][0] = Number(strikeRate[year][data[index]['batsman']][0]) + Number(data[index]['batsman_runs'])
                        strikeRate[year][data[index]['batsman']][1] = 1 + strikeRate[year][data[index]['batsman']][1]
                    }
                }
            }
            for (let player in strikeRate[year]) {
                strikeRate[year][player] = ((strikeRate[year][player][0]) * 100/ strikeRate[year][player][1]).toFixed(3);
            }

        }
        fs.writeFile('src/public/outputs/strick_rate.json', JSON.stringify(strikeRate))
    } catch (error) {
        console.log(error)
    }
}


function getTheMatchIds(data) {
    try {
        let matchIds = {}
        for (let index = 0; index < data.length; index++) {
            let tempDate = new Date(data[index]['date'])
            tempDate = tempDate.getFullYear()
          if(tempDate)
            {  if (!matchIds[tempDate]) {
                console.log(tempDate)
                matchIds[tempDate] = []
            }
            matchIds[tempDate].push(data[index]['id'])
        }
           
        }
        ids = matchIds
        readCsvFile.readCsvFile(getStrikeRateOfEveryPlayer, 'src/data/deliveries.csv')
    } catch (error) {
        console.log(error)
    }
}