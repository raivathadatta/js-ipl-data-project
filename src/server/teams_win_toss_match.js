// ind the number of times each team won the toss and also won the match

let readCsvFile = require('../../util.js')
let fs = require('file-system')

readCsvFile.readCsvFile(getTheTeamsWOnTossAndMatch)

function getTheTeamsWOnTossAndMatch(data) {
    try {
        if (!Array.isArray(data)) {
            throw 'input miss match expecte array'
        }
        let teamsWinTossAndMatch = {}
        for (let index = 0; index < data.length; index++) {
            if (data[index]['winner']) {
                if (data[index]['winner'] == data[index]['toss_winner']) {
                    if (!teamsWinTossAndMatch[data[index]['winner']]) {
                        teamsWinTossAndMatch[data[index]['winner']] = 0
                    }
                    teamsWinTossAndMatch[data[index]['winner']] = 1 + teamsWinTossAndMatch[data[index]['winner']]
                }

            }
        }
        let teamsWon = Object.keys(teamsWinTossAndMatch)
        for(let teams in teamsWinTossAndMatch ){
            for(let index=0;index<teamsWon.length;index++){
                if(teamsWon[index].includes(teams)){
                   if(teamsWon[index] != teams){
                    console.log('hello')
                    teamsWinTossAndMatch[teamsWon[index]] = Number(teamsWinTossAndMatch[teamsWon[index]]) + Number(teamsWinTossAndMatch[teams])
                    delete teamsWinTossAndMatch[teams]
                   }
                }
            }

        }
        
        console.log(teamsWinTossAndMatch)
        fs.writeFile('src/public/outputs/teams_win_toss_match.json', JSON.stringify(teamsWinTossAndMatch),)

    } catch (error) {
        console.log(error)
    }
}