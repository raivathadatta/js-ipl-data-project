const fs = require('file-system')

function readCsvFile(cd,path = './src/data/matches.csv') {
try{
    if(!typeof cd == 'function'){
        throw 'input miss match expected function'
    }
    fs.readFile(path, 'UTF-8', (err, data) => {
      if(err){
        throw `${err}`
      }else{
        csvToArray(data,cd)
      }
    })

}catch(err){
    console.log(err)
}
}
function csvToArray(data,cd) {
    
  try {
    const [header, ...lines] = data.split('\n')
    let headObject = header.replace('\r','').split(',')
    console.log(headObject.length)
    console.log(Array.isArray(lines))
    output = []
    for(let indexLines =0;indexLines<lines.length;indexLines++){
      lines[indexLines] = lines[indexLines].replace('\r','').split(',')
      let temp = {}
      for (let index = 0; index < headObject.length; index++) {
          temp[headObject[index]] = lines[indexLines][index]
      }
      output.push(temp)
    }
    console.log(output[0])
    
    return cd(output)
  } catch (error) {
   
    console.log(cd)
    console.log(error)
  }
}


 module.exports = {readCsvFile:readCsvFile}





